import { Controller, UseGuards, Get, Post, Body, Put, Param } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ctts } from '../../util/constants';
import { ApiBearerAuth, ApiOperation } from '@nestjs/swagger';
import { TarjetaService } from '../../services/tarjeta/tarjeta.service';
import { TarjetaDto } from '../../models/dtos/tarjeta.dto';
import { TarjetaOficina } from '../../decorators/tarjeta-oficina.decorators';
import { TarjetaOficinaDto } from '../../models/dtos/tarjeta-oficina.dto';
import { MovimientoDto } from '../../models/dtos/movimiento.dto';

@Controller('tarjeta')
@UseGuards(AuthGuard(ctts.keys.strategyJwtName))
@ApiBearerAuth()
export class TarjetaController {
    constructor(
        private readonly tarjetaService: TarjetaService
    ) { }

    @Get('stock')
    @ApiOperation({ title: 'Metodo de listado de tarjetas para el stock' })
    async listarStockTarjetas(@TarjetaOficina() tarjetaOfinaDto: TarjetaOficinaDto) {
        return await this.tarjetaService.listarTarjetasOficinas(tarjetaOfinaDto);
    }

    @Get()
    @ApiOperation({ title: 'Metodo de listado de tarjetas' })
    async listarTarjetas() {
        return await this.tarjetaService.listarTarjetas();
    }

    @Post('create')
    @ApiOperation({ title: 'Metodo de creacion de tarjeta' })
    async crearTarjeta(@Body() tarjetaDto: TarjetaDto) {
        return await this.tarjetaService.crearTarjeta(tarjetaDto);
    }

    @Put('stock/update/:id')
    @ApiOperation({ title: 'Metodo que modifica los datos de la tarjeta relacionada a la oficna' })
    async modificarTarjetaOficina(@Param() params, @Body() tarjetaOficnaDto: TarjetaOficinaDto) {
        tarjetaOficnaDto._id = params.id;
        return await this.tarjetaService.modificarTarjetaOficina(tarjetaOficnaDto);
    }

    @Get('stock/movimientos/:id')
    @ApiOperation({ title: 'Metodo que obtiene los movimeintos de una tarjeta relacionada a la oficna' })
    async listarMovimientos(@Param() params) {
        return await this.tarjetaService.listarMovimientos(params.id);
    }

    @Post('stock/movimientos')
    @ApiOperation({ title: 'Creacion de nuevo movimiento' })
    async crearMovimiento(@Body() movimientoDto:MovimientoDto) {
        return await this.tarjetaService.crearMovimiento(movimientoDto);
    }

    @Get('stock/:id')
    @ApiOperation({ title: 'Metodo que obtiene el stock actal de una tarjeta asignada a una oficina' })
    async obtenerStockActual(@Param() params) {
        return await this.tarjetaService.obtenerStockActual(params.id);
    }

    /**ADQUIRIR UNA TARJETA**/

    @Post('adquirirtarjeta')
    @ApiOperation({ title: 'Metodo de aquisición de tarjeta' })
    async adquirirTarjeta(@Body() tarjetaOficinaDto: TarjetaOficinaDto) {
        return await this.tarjetaService.adquirirTarjeta(tarjetaOficinaDto);
    }

}
