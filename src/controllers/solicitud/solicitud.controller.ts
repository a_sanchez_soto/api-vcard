import { Controller, UseGuards, Get, Post, Body, Put, Param } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ctts } from '../../util/constants';
import { ApiBearerAuth, ApiOperation } from '@nestjs/swagger';
import { SolicitudService } from '../../services/solicitud/solicitud.service';
import { SolicitudDto } from '../../models/dtos/solicitud.dto';
import { Solicitud } from '../../decorators/solicitud.decorators';
import { ParametroService } from 'src/services/parametro/parametro.service';
@Controller('solicitud')
@UseGuards(AuthGuard(ctts.keys.strategyJwtName))
@ApiBearerAuth()
export class SolicitudController {


constructor( 
    private readonly solicitudService: SolicitudService
    ){}

    @Post()
    @ApiOperation({ title: 'Metodo de creacion de solicitud de pedido de tarjetas' })
    async crearSolicitudPedido(@Body() solicitudDto: SolicitudDto) {
        return await this.solicitudService.crearSolicitudPedido(solicitudDto);
    }

    @Get()
    @ApiOperation({ title: 'Metodo que obtiene las solicitudes' })
    async listarSolicitudes(@Solicitud() solicitudDto: SolicitudDto) {
        return await this.solicitudService.listarSolicitudes(solicitudDto);
    }

    @Put(':codigo')
    @ApiOperation({ title: 'Metodo de modificacion de solicitud de pedido de tarjetas' })
    async modificarSolicitudPedido(@Param() params, @Body() solicitudDto: SolicitudDto) {
        solicitudDto.codigo = params.codigo;
        return await this.solicitudService.modificarSolicitudPedido(solicitudDto);
    }

    @Put('promover/:codigo')
    @ApiOperation({ title: 'Metodo para cambiar estado a la solicitud (Promover)' })
    async crearTarjeta(@Param() params, @Body() solicitudDto: SolicitudDto) {
        solicitudDto.codigo = params.codigo;
        return await this.solicitudService.cambiarEstado(solicitudDto);
    }

    @Put('anular/:codigo')
    @ApiOperation({ title: 'Metodo que anula solicitud de pedido de tarjetas' })
    async anularSolicitud(@Param() params, @Body() solicitudDto: SolicitudDto) {
        solicitudDto.codigo = params.codigo;
        return await this.solicitudService.anularSolicitud(solicitudDto);
    }

    @Get('job/gestion')
    @ApiOperation({ title: 'Job Promover selicitudes a gestion' })
    async promoverSolicitudesAGestion() {
        return await this.solicitudService.promoverSolicitudesAGestion();
    }

    
    @Get('job/produccion')
    @ApiOperation({ title: 'Job Promover selicitudes a produccion' })
    async promoverSolicitudesAProduccion() {
        return await this.solicitudService.promoverSolicitudesAProduccion();
    }

    @Get('job/solicitudAutomatica')
    @ApiOperation({ title: 'Job de generación de solicitud de tarjeta' })
    async generarSolicitudAutomatica() {
        return await this.solicitudService.generacionSolicitudJob();
    }

}
