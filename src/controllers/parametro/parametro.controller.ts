import { Controller, Post, Body, UseGuards, Get, Param, Delete, Put } from '@nestjs/common';
import { ParametroService } from '../../services/parametro/parametro.service';
import { ApiOperation, ApiBearerAuth } from '@nestjs/swagger';
import { ListaDto } from '../../models/dtos/lista.dto';
import { AuthGuard } from '@nestjs/passport';
import { ctts } from '../../util/constants';
import { ValorDto } from '../../models/dtos/valor.dto';

@Controller('parametro')
@UseGuards(AuthGuard(ctts.keys.strategyJwtName))
@ApiBearerAuth()
export class ParametroController {

    constructor(
        private readonly parametroService: ParametroService
    ) { }

    @Post('lista')
    @ApiOperation({ title: 'Metodo de creacion de lista de valores' })
    async crearLista(@Body() listaDto: ListaDto) {
        return await this.parametroService.crearLista(listaDto);
    }

    @Put('lista/:id')
    @ApiOperation({ title: 'Metodo que modifica una lista de valores' })
    async modificarLista(@Param() params, @Body() listaDto: ListaDto) {
        return await this.parametroService.modificarLista(params.id, listaDto);
    }

    @Get('lista')
    @ApiOperation({ title: 'Metodo de listado de lista de valores' })
    async listarLista() {
        return await this.parametroService.listarLista();
    }

    @Get('lista/:codigo')
    @ApiOperation({ title: 'Metodo que obtiene una lista por codigo' })
    async obtenerLista(@Param() params) {
        return await this.parametroService.obtenerListaPorCodigo(params.codigo)
    }

    @Delete('lista/:codigo')
    @ApiOperation({ title: 'Metodo que anula una lista de valores' })
    async anularLista(@Param() params) {
        return await this.parametroService.eliminarLista(params.codigo)
    }

    @Post('valor')
    @ApiOperation({ title: 'Metodo de creacion valor de lista' })
    async crearValor(@Body() valorDto: ValorDto) {
        return await this.parametroService.crearValor(valorDto);
    }

    @Get(':lista/valores')
    @ApiOperation({ title: 'Metodo de listado de valores por lista' })
    async listarValores(@Param() params) {
        return await this.parametroService.listarValoresPorLista(params.lista)
    }

}
