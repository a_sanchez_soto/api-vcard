import { Controller, UseGuards, Post, Body, Param, Put, Get } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ctts } from '../../util/constants';
import { ApiBearerAuth, ApiOperation } from '@nestjs/swagger';
import { NotificacionService } from '../../services/notificacion/notificacion.service';
import { NotificacionDto } from '../../models/dtos/noticication.dto';

@Controller('notificacion')
@UseGuards(AuthGuard(ctts.keys.strategyJwtName))
@ApiBearerAuth()
export class NotificacionController {

    constructor(
        private readonly notificacionService: NotificacionService
    ) { }

    @Post('emit')
    @ApiOperation({ title: 'Creacion y emision de notificacion por oficina' })
    async crearNotificacion(@Body() notificacionDto: NotificacionDto) {
        return await this.notificacionService.crearNotificacion(notificacionDto);
    }

    @Put('check/:id')
    @ApiOperation({ title: 'Marcar vista la notificacion' })
    async checkNotificacion(@Param() params) {
        return await this.notificacionService.checkNotificacion(params.id);
    }

    @Get('oficina/:idOficina')
    @ApiOperation({ title: 'Listar notificaciones por oficina' })
    async listarNotificacion(@Param() params) {
        return await this.notificacionService.listarNotificacionesPorOficina(params.idOficina);
    }

}
