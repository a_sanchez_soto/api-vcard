import { Module } from '@nestjs/common';
import { ConfigEnvModule } from './config/env/configenv.module';
import { UsuarioModule } from './modules/usuario/usuario.module';
import { AuthModule } from './modules/auth/auth.module';
import { APP_FILTER } from '@nestjs/core';
import { HttpErrorFilter } from './filters/http-error.filter';
import { LogModule } from './log/log.module';
import { ParametroModule } from './modules/parametro/parametro.module';
import { TarjetaModule } from './modules/tarjeta/tarjeta.module';
import { SolicitudModule } from './modules/solicitud/solicitud.module';
import { SequenceModule } from './modules/sequence/sequence.module';
import { ScheduleModule } from 'nest-schedule';
import { ScheduleTarjetaModule } from './modules/schedule-tarjeta/schedule-tarjeta.module';
import { AppGateway } from './gateways/app.gateway';
import { NotificacionModule } from './modules/notificacion/notificacion.module';

@Module({
  imports: [
    ConfigEnvModule,
    UsuarioModule,
    AuthModule,
    LogModule,
    ParametroModule,
    TarjetaModule,
    SolicitudModule,
    SequenceModule,
    ScheduleTarjetaModule,
    ScheduleModule.register({}),
    NotificacionModule,
  ],
    providers: [
      {
        provide: APP_FILTER,
        useClass: HttpErrorFilter,
      },
      AppGateway
    ]
})
export class AppModule { }
