import { NestFactory, APP_FILTER } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as helmet from 'helmet';
import { UsuarioModule } from './modules/usuario/usuario.module';
import { LogService } from './log/log.service';
import { ParametroModule } from './modules/parametro/parametro.module';
import { TarjetaModule } from './modules/tarjeta/tarjeta.module';
import { SolicitudModule } from './modules/solicitud/solicitud.module';

function applySecurity(app) {
  app.use(helmet());
  app.enableCors({
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
    "preflightContinue": false,
    "optionsSuccessStatus": 204
  });
}

function applyOpenApi(app) {
  const hostOpenAPi = 'open-api';

  const optionsUsuario = new DocumentBuilder()
    .setTitle('Usuarios')
    .setDescription('API de servicios de usuarios')
    .setVersion('1.0')
    .addBearerAuth('Authorization', 'header', 'apiKey')
    .build();

  const usuarioDocument = SwaggerModule.createDocument(app, optionsUsuario, {
    include: [UsuarioModule],
  });
  SwaggerModule.setup(`${hostOpenAPi}/usuarios`, app, usuarioDocument);

  const optionsParametros = new DocumentBuilder()
    .setTitle('Parametros')
    .setDescription('API de servicios de Parametros')
    .setVersion('1.0')
    .addBearerAuth('Authorization', 'header', 'apiKey')
    .build();

  const parametrosDocument = SwaggerModule.createDocument(app, optionsParametros, {
    include: [ParametroModule],
  });
  SwaggerModule.setup(`${hostOpenAPi}/parametros`, app, parametrosDocument);


  const optionsTarjeta = new DocumentBuilder()
    .setTitle('Tarjetas')
    .setDescription('API de servicios de Tarjetas')
    .setVersion('1.0')
    .addBearerAuth('Authorization', 'header', 'apiKey')
    .build();

  const tarjetasDocument = SwaggerModule.createDocument(app, optionsTarjeta, {
    include: [TarjetaModule],
  });
  SwaggerModule.setup(`${hostOpenAPi}/tarjetas`, app, tarjetasDocument);

  const optionsSolicitud = new DocumentBuilder()
    .setTitle('Solicitudes')
    .setDescription('API de servicios de Solicitudes')
    .setVersion('1.0')
    .addBearerAuth('Authorization', 'header', 'apiKey')
    .build();

  const solicitudesDocument = SwaggerModule.createDocument(app, optionsSolicitud, {
    include: [SolicitudModule],
  });
  SwaggerModule.setup(`${hostOpenAPi}/solicitudes`, app, solicitudesDocument);


}

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: new LogService(),
  });
  applySecurity(app);
  applyOpenApi(app);
  await app.listen(3001);
}
bootstrap();
