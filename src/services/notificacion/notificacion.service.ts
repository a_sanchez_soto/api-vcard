import { Injectable, Inject } from '@nestjs/common';
import { ctts } from '../../util/constants';
import { INotificacion } from '../../models/interfaces/notificacion.interface';
import { Model } from 'mongoose';
import { LogService } from '../../log/log.service';
import { NotificacionDto } from '../../models/dtos/noticication.dto';
import { AppGateway } from '../../gateways/app.gateway';

@Injectable()
export class NotificacionService {

    constructor(
        @Inject(ctts.db.models.notificacion.provide)
        private readonly notificacionModel: Model<INotificacion>,
        private readonly logService: LogService,
        private readonly appGateway: AppGateway
    ) {}

    async obtenerNotificacionPorId(id) {
        return await this.notificacionModel.findOne({ _id: id });
    }

    async crearNotificacion(notificacionDto: NotificacionDto) {
        notificacionDto.creacion = new Date();
        notificacionDto.visto = '0';
        this.logService.log(`Creacion notificacion: ${JSON.stringify(notificacionDto)}`)
        let notificacion = new this.notificacionModel(notificacionDto);
        await notificacion.save();
        let result = await this.obtenerNotificacionPorId(notificacion._id);
        this.logService.log(`CreacionEmitirMensajeWebSocket: ${JSON.stringify(result)}`);
        this.appGateway.wsServer.emit('messageToClient',JSON.stringify(result));
        return result;
    }

    async checkNotificacion(notificacionId) {
        let notificacionDto = new NotificacionDto();
        notificacionDto.visto = '1';
        let notificacion = await this.notificacionModel.findOneAndUpdate({_id: notificacionId}, notificacionDto);
        this.appGateway.wsServer.emit('messageToClient',JSON.stringify(notificacion));
        return await notificacion;
    }

    async listarNotificacionesPorOficina(oficinaId) {
        return await this.notificacionModel.find({ oficina: oficinaId , visto: '0'}).exec();
    }

}
