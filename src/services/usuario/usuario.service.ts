import { Injectable, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { ctts } from '../../util/constants';
import { IUsuario } from '../../models/interfaces/usuario.interface';
import { Model } from 'mongoose';
import { UsuarioDto } from '../../models/dtos/usuario.dto';
import * as bcrypt from 'bcryptjs';
import { LogService } from '../../log/log.service';

@Injectable()
export class UsuarioService {

    constructor(
        @Inject(ctts.db.models.usuario.provide)
        private readonly usuarioModel: Model<IUsuario>,
        private readonly logService: LogService
    ) { }

    async listarUsuarios() {
        return await this.usuarioModel.find()
        .populate(ctts.populates.oficina)
        .populate(ctts.populates.perfil)
        .populate(ctts.populates.estado)
        .populate(ctts.populates.opciones)
        .exec();
    }

    async crearUsuario(usuarioDto: UsuarioDto) {
        const exiteUsuario = await this.obtenerUsuarioPorUserName(usuarioDto.registro);
        if(exiteUsuario) {
            throw new HttpException(`El usuario: ${usuarioDto.userName}, ya existe.`, HttpStatus.CONFLICT);
        }
        usuarioDto.password = await bcrypt.hash(usuarioDto.password, ctts.keys.hashLevelBcrypt);
        usuarioDto.userName = usuarioDto.registro;
        this.logService.log(`Bpdy usuario insert: ${JSON.stringify(usuarioDto)}`)
        let usuario = new this.usuarioModel(usuarioDto);
        await usuario.save();
        return await this.obtenerUsuarioPorId(usuario._id);
    }

    async modificarUsuario(id: string,usuarioDto: UsuarioDto) {
        await this.usuarioModel.updateOne({ _id: id }, usuarioDto);
        let usuario = await this.obtenerUsuarioPorId(id);
        return { data: usuario };
    }

    async obtenerUsuarioPorId(id: string) {
        return await this.usuarioModel.findOne({ _id: id })
                        .populate(ctts.populates.oficina)
                        .populate(ctts.populates.perfil)
                        .populate(ctts.populates.estado)
                        .populate(ctts.populates.opciones);
    }

    async obtenerUsuarioPorUserName(userName: string) {
        return await this.usuarioModel.findOne({ userName: { $regex: new RegExp(userName , 'ig') } })
            .populate(ctts.populates.oficina)
            .populate(ctts.populates.perfil)
            .populate(ctts.populates.estado)
            .populate(ctts.populates.opciones);
    }

    async eliminarUsuario(id: string) {
        const usuarioEliminado = await this.usuarioModel.findOneAndRemove({ _id: id });
        if(usuarioEliminado) {
            return usuarioEliminado;
        } else {
            throw new HttpException('El usuario no existe.', HttpStatus.CONFLICT);
        }
         
    }

}
