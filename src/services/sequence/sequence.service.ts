import { Injectable, Inject } from '@nestjs/common';
import { ctts } from '../../util/constants';
import { Model } from 'mongoose';
import { ISequence } from '../../models/interfaces/sequence.interface';
import { LogService } from '../../log/log.service';

@Injectable()
export class SequenceService {

    constructor(
        @Inject(ctts.db.models.sequence.provide)
        private readonly sequenceModel: Model<ISequence>,
        private readonly logService: LogService
    ) {}

    async getNextSequenceValue(sequenceName: string) {
        let seq = await this.getSequenceByName(sequenceName);
        this.logService.log(`La sequencia existe: ${JSON.stringify(seq)}`);
        if(!seq){
            let newSeq = {
                _id : sequenceName,
                sequence_value: 1
            }
            let createSeq = new this.sequenceModel(newSeq);
            let responseCreate = await createSeq.save();
            this.logService.log(`Sequencia[${sequenceName}] creada valor = ${newSeq.sequence_value}`);
            return newSeq.sequence_value;
        }else{
            let increment = (seq.sequence_value+1);
             await this.sequenceModel.updateOne({ _id: sequenceName }, { sequence_value: increment  });
             this.logService.log(`Sequencia[${sequenceName}] existe valor = ${increment}`);
             return increment;
        }
        
    }    

    async getSequenceByName(sequenceName) {
        return await this.sequenceModel.findOne({ _id: sequenceName });
    }


}
