import { Injectable, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { ctts } from '../../util/constants';
import { Model } from 'mongoose';
import { ILista } from '../../models/interfaces/lista.interface';
import { ListaDto } from '../../models/dtos/lista.dto';
import { IValor } from '../../models/interfaces/valor.interface';
import { ValorDto } from '../../models/dtos/valor.dto';
import { LogService } from '../../log/log.service';

@Injectable()
export class ParametroService {

    constructor(
        @Inject(ctts.db.models.lista.provide)
        private readonly listaModel: Model<ILista>,
        @Inject(ctts.db.models.valor.provide)
        private readonly valorModel: Model<IValor>,
        private readonly logService: LogService
    ) {}

    async crearLista(listaDto: ListaDto) {
        let existeLista = await this.obtenerListaPorCodigo(listaDto.codigo);
        if(existeLista) {
            throw new HttpException(`La Lista con código: ${listaDto.codigo}, ya existe.`, HttpStatus.CONFLICT);
        }

        if(typeof listaDto.codigo === 'undefined' || listaDto.codigo === null || listaDto.codigo === '' ) {
            throw new HttpException(`No se informó el codigo de la lista.`, HttpStatus.CONFLICT);
        }

        listaDto.creacion = new Date();
        listaDto.estado = ctts.states.on;
        listaDto.codigo = listaDto.codigo.toUpperCase(); 
        let lista = new this.listaModel(listaDto);
        return await lista.save();
    }

    async listarLista() {
        return await this.listaModel.find({ estado: ctts.states.on }).exec();
    }

    async modificarLista(id: string, listaDto: ListaDto) {
        await this.listaModel.updateOne({ _id: id }, listaDto);
        let lista = await this.obtenerListaPorId(id);
        return { data: lista };
    }

    async eliminarLista(id: string) {
        let listaDto: ListaDto = new ListaDto();
        listaDto.estado = ctts.states.off;
        return await this.modificarLista(id, listaDto);
    }

    async obtenerListaPorCodigo(codigo) {
        return await this.listaModel.findOne({ codigo: codigo }); 
    }

    async obtenerListaPorId(id) {
        return await this.listaModel.findOne({ _id: id }); 
    }

    async crearValor(valorDto: ValorDto){
        let existeValor = await this.obtenerValorPorCodigo(valorDto.codigo);
        if(existeValor) {
            throw new HttpException(`El Valor con código: ${valorDto.codigo}, ya existe.`, HttpStatus.CONFLICT);
        }
        if(typeof valorDto.codigo === 'undefined' || valorDto.codigo === null || valorDto.codigo === '' ) {
            throw new HttpException(`No se informó el codigo del valor.`, HttpStatus.CONFLICT);
        }
        let valor = new this.valorModel(valorDto);
        valorDto.codigo = valorDto.codigo.toUpperCase();
        return await valor.save();
    }

    async modificarValor(id: string, valorDto: ValorDto) {
        await this.valorModel.updateOne({ _id: id }, valorDto);
        let valor = await this.obtenerValorPorId(id);
        return { data: valor };
    }

    async listarValoresPorLista(codlista: string) {
        this.logService.log(`listarValoresPorLista() [Codigo de lista = ${codlista}]`);
        let lista = await this.obtenerListaPorCodigo(codlista);
        this.logService.log(`lista(${codlista}) = ${JSON.stringify(lista)}]`);
        return await this.valorModel.find({ lista: lista._id }).populate(ctts.populates.lista);
    }

    async obtenerValorPorCodigo(codigo) {
        return await this.valorModel.findOne({ codigo: codigo }).populate(ctts.populates.lista);; 
    }

    async obtenerValorPorId(id) {
        return await this.valorModel.findOne({ _id: id }).populate(ctts.populates.lista);; 
    }

}
