import { Injectable, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { ctts } from '../../util/constants';
import { Model } from 'mongoose';
import { ISolicitud } from '../../models/interfaces/solicitud.interface';
import { SolicitudDto } from '../../models/dtos/solicitud.dto';
import { IDetalle } from '../../models/interfaces/detalle.interface';
import { LogService } from '../../log/log.service';
import { SequenceService } from '../sequence/sequence.service';
import { IMovimiento } from '../../models/interfaces/movimiento.interfcae';
import { ParametroService } from '../parametro/parametro.service';
import { UsuarioService } from '../usuario/usuario.service';
import { ITarjetaOfciina } from '../../models/interfaces/tarjeta-oficina.interface';
import { TarjetaOficinaDto } from '../../models/dtos/tarjeta-oficina.dto';
import { MovimientoDto } from '../../models/dtos/movimiento.dto';
import { TarjetaService } from '../tarjeta/tarjeta.service';
import { DetalleDto } from '../../models/dtos/detalle.dto';
import { AppGateway } from '../../gateways/app.gateway';
import { NotificacionService } from '../notificacion/notificacion.service';
import { NotificacionDto } from '../../models/dtos/noticication.dto';

@Injectable()
export class SolicitudService {

    constructor(
        @Inject(ctts.db.models.solicitud.provide)
        private readonly solicitudModel: Model<ISolicitud>,
        @Inject(ctts.db.models.detalle.provide)
        private readonly detalleModel: Model<IDetalle>,
        private readonly logService: LogService,
        private readonly sequenceService: SequenceService,
        private readonly parametroService: ParametroService,
        private readonly usuarioService: UsuarioService,
        private readonly tarjetaService: TarjetaService,
        private readonly notificacionService: NotificacionService
   
    ) { }

    

    async listarSolicitudes(solicitudDto) {
        let usuario = await this.usuarioService.obtenerUsuarioPorId(solicitudDto.idUsuario)
        let { idUsuario, ...filtro } = solicitudDto;
        if (!usuario) {
            usuario = {
                perfil: {}
            }
        }
        this.logService.log(`Usuario perfil: ${JSON.stringify(usuario.perfil)}`)
        if (usuario.perfil.codigo === ctts.keys.perfiles.OFICINA) {
            filtro.oficina = usuario.oficina._id;
        }
        this.logService.log(`Listar solicitudes with paramas: ${JSON.stringify(filtro)}`)
        let solicitudes = await this.solicitudModel.find(filtro)
            .populate(ctts.populates.estado)
            .populate(ctts.populates.oficina)
            .populate(ctts.populates.territorio)
            .populate(ctts.populates.usuarioCreador)
            .populate(ctts.populates.usuarioAsignado)
            .populate(ctts.populates.motivo)
            .populate({
                path: ctts.populates.details,
                populate: {
                    path: ctts.populates.tarjeta
                }
            })
            .sort([['codigo', 'descending']])
            .exec();

        this.logService.log(`Total de solicitudes: ${solicitudes.length}`)
        if (usuario.perfil.codigo === ctts.keys.perfiles.PROVEEDOR &&
            solicitudes && solicitudes.filter) {
            solicitudes = solicitudes.filter((item) => {

                return (item.estado.codigo !== ctts.keys.estados.SOLICITADO
                    && item.estado.codigo !== ctts.keys.estados.ANULADO)
            });
            this.logService.log(`Total de solicitudes Filtradas perfil proveedor: ${solicitudes.length}`)
        }

        return solicitudes;
    }


    async crearDetalle(detail) {
        let detalle = new this.detalleModel(detail);
        this.logService.log(`crearDetalle[detail]=${JSON.stringify(detail)}`);
        return await detalle.save();
    }

    async crearDetalleMultiple(details, solicitudId) {
        let inserts = [];
        for (let index = 0; index < details.length; index++) {
            details[index].solicitud = solicitudId;
            let result = await this.crearDetalle(details[index]);
            inserts[index] = result._id;
        }
        this.logService.log(`Done! inserts: ${inserts}`);
        return inserts;
    }

    async modificarSolicitudPedido(solicitudDto: SolicitudDto) {
        let solicitud = await this.obtenerSolictudPorCodigo(solicitudDto.codigo);
        if (solicitud.estado.codigo !== ctts.keys.estados.SOLICITADO) {
            throw new HttpException('Solo pueden modificarse las solicitudes que se encuentren en estado solicitado.', HttpStatus.CONFLICT);
        }
        try {
            const session = await this.solicitudModel.db.startSession();
            session.startTransaction();
            try {
                await this.detalleModel.remove({ solicitud: solicitud._id });
                solicitud.ultimaModificacion = new Date();
                solicitud.details = await this.crearDetalleMultiple(solicitudDto.details, solicitud._id);
                await solicitud.save();
                return await this.obtenerSolictudPorCodigo(solicitud.codigo);
            } catch (error) {
                await session.abortTransaction();
                this.logService.error('Error al modificar la solitud', error);
            } finally {
                session.endSession();
            }
        } catch (error) {
            this.logService.error('Transaccion no establecida', error);
        }
    }

    async crearSolicitudPedido(solicitudDto: SolicitudDto) {
        try {
            const session = await this.solicitudModel.db.startSession();
            session.startTransaction();
            try {
                solicitudDto.codigo = await this.sequenceService.getNextSequenceValue(ctts.db.models.solicitud.entityName);
                solicitudDto.creacion = new Date();
                this.logService.log(`solicitudDto.codigo[generate]: ${solicitudDto.codigo}`);
                this.logService.log(`solicitudDto[bodyCreate]: ${JSON.stringify(solicitudDto)}`);
                let estadoSolicitado = await this.parametroService.obtenerValorPorCodigo(ctts.keys.estados.SOLICITADO);
                this.logService.log(`Estado solicitado[${ctts.keys.estados.SOLICITADO}]: ${JSON.stringify(estadoSolicitado)}`);
                let solicitudCreate = new SolicitudDto();
                solicitudCreate.estado = estadoSolicitado._id;
                solicitudCreate.oficina = solicitudDto.oficina;
                solicitudCreate.usuarioAsignado = solicitudDto.usuarioAsignado;
                solicitudCreate.usuarioCreador = solicitudDto.usuarioCreador;
                solicitudCreate.codigo = solicitudDto.codigo;
                solicitudCreate.creacion = solicitudDto.creacion;
                this.logService.log(`Body a insertar: ${JSON.stringify(solicitudCreate)}`);
                let solicitud = new this.solicitudModel(solicitudCreate);
                await solicitud.save();
                solicitud.details = await this.crearDetalleMultiple(solicitudDto.details, solicitud._id);
                this.logService.log(`solicitud.details: ${JSON.stringify(solicitud.details)}`);
                await solicitud.save();
                let solicitudCreada = await this.obtenerSolictudPorCodigo(solicitud.codigo);
                this.logService.log(`Solicitud creada: ${JSON.stringify(solicitudCreada)}`);
                return solicitudCreada;
            } catch (error) {
                await session.abortTransaction();
                this.logService.error('Error al crear la solitud', error);
            } finally {
                session.endSession();
            }
        } catch (error) {
            this.logService.error('Transaccion no establecida', error);
        }

    }

    async obtenerSolictudPorCodigo(codigo) {
        let solicitud = await this.solicitudModel.findOne({ codigo: codigo })
            .populate(ctts.populates.estado)
            .populate(ctts.populates.oficina)
            .populate(ctts.populates.territorio)
            .populate(ctts.populates.usuarioCreador)
            .populate(ctts.populates.usuarioAsignado)
            .populate(ctts.populates.motivo)
            .populate({
                path: ctts.populates.details,
                populate: {
                    path: ctts.populates.tarjeta
                }
            })
        return solicitud;
    }

    async cambiarEstado(solicitudDto: SolicitudDto) {
        this.logService.log(`Parametros para cambio de estado: ${JSON.stringify(solicitudDto)}`);
        let solicitudValidar = await this.solicitudModel.findOne({ codigo: solicitudDto.codigo });
        let estadoSolicitud = await this.parametroService.obtenerValorPorId(solicitudValidar.estado);
        let estadoAPromover = await this.parametroService.obtenerValorPorId(solicitudDto.estado);

        if (estadoSolicitud.codigo === ctts.keys.estados.ANULADO
            || estadoSolicitud.codigo === ctts.keys.estados.ENTREGADO) {
           // throw new HttpException('No se pueden promover solicitudes anuladas o ya entregadas.', HttpStatus.CONFLICT);
        }

        try {
            const session = await this.solicitudModel.db.startSession();
            session.startTransaction();

            try {
                this.logService.log('========================INICIO PROMOVER=========================');
                let updated = new SolicitudDto();
                updated.estado = solicitudDto.estado;
                let solicitud = await this.solicitudModel
                    .findOneAndUpdate({ codigo: solicitudDto.codigo }, updated)
                    .populate(ctts.populates.estado)
                    .populate(ctts.populates.oficina)
                    .populate({
                        path: ctts.populates.details,
                        populate: {
                            path: ctts.populates.tarjeta
                        }
                    });

                this.logService.log(`Solicitud modificada: ${JSON.stringify(solicitud)}`);
                this.logService.log(`estadoAPromover Parametro: ${estadoAPromover.codigo}`);
                if (estadoAPromover.codigo === ctts.keys.estados.ENTREGADO) {
                    this.logService.log(`Oficina de promocion[codigo:${solicitud.oficina.codigo}]: ${solicitud.oficina._id}`);
                    this.logService.log(`Cantidad Detalle Promover: ${solicitud.details.length}`);
                    let tipoMovimientoIngreso = await this.parametroService.obtenerValorPorCodigo(ctts.keys.tipoMovimiento.in);
                    this.logService.log(`tipoMovimientoIngreso: ${tipoMovimientoIngreso.codigo}`);
                    let tarjetaOficina = null;
                    let tarjetaOficinaDto = null;
                    let movimiento = null;
                    let movimientoDto = null;
                    for (let i = 0; i < solicitud.details.length; i++) {
                        const detail = solicitud.details[i];
                        this.logService.log(`Pmomover.solicitud.detail[${i}]: ${JSON.stringify(detail)}`);
                        tarjetaOficina = await this.tarjetaService.obtenerTarjetaOficina(detail.tarjeta._id, solicitud.oficina._id);
                        if (!tarjetaOficina || !tarjetaOficina._id) {
                            this.logService.log('No existe Tarjeta Oficina: se registra como nuevo');
                            tarjetaOficinaDto = new TarjetaOficinaDto();
                            tarjetaOficinaDto.tarjeta = detail.tarjeta;
                            tarjetaOficinaDto.oficina = solicitud.oficina;
                            this.logService.log(`tarjetaOficinaDTO: ${JSON.stringify(tarjetaOficinaDto)}`);
                            tarjetaOficina = await this.tarjetaService.crearTarjetaOficina(tarjetaOficinaDto);
                        }
                        this.logService.log('Si existe Tarjeta Oficina: Se registra movimiento de ingreso(+)');
                        this.logService.log(`tarjetaOficina: ${JSON.stringify(tarjetaOficina)}`);
                        movimientoDto = new MovimientoDto();
                        movimientoDto.tipoMovimiento = tipoMovimientoIngreso._id;
                        movimientoDto.cantidad = detail.cantidad;
                        movimientoDto.tarjetaOficina = tarjetaOficina._id;
                        movimiento = await this.tarjetaService.crearMovimiento(movimientoDto);
                        this.logService.log(`Movimiento creado: ${JSON.stringify(movimiento)}`);
                        await this.tarjetaService.agregarMovimientoTarjetaOficina(tarjetaOficina, movimiento)
                        //Se asigna false a pedidoAutomaticoEnviado si esta en true y 
                        //y al tarjeta esta configurada con pedido automatico
                        if(tarjetaOficina.automatico && tarjetaOficina.pedidoAutomaticoEnviado) {
                            tarjetaOficina.pedidoAutomaticoEnviado = true;
                            await tarjetaOficina.save();
                        }
                    }
                }
                this.logService.log('=========================FIN PROMOVER==========================');
                return solicitud;
            } catch (error) {
                await session.abortTransaction();
                this.logService.error('Error al actualizar Estado de solicitud', error);
            } finally {
                session.endSession();
            }
        } catch (error) {
            this.logService.error('Transaccion no establecida', error);
        }
    }

    async anularSolicitud(solicitudDto: SolicitudDto) {
        this.logService.log('Inicio ejecucion anularSolicitud()');
        let solicitud = await this.obtenerSolictudPorCodigo(solicitudDto.codigo);
        if (solicitud.estado.codigo !== ctts.keys.estados.SOLICITADO) {
            throw new HttpException('Solo pueden modificarse las solicitudes que se encuentren en estado solicitado.', HttpStatus.CONFLICT);
        }

        try {
            const session = await this.solicitudModel.db.startSession();
            session.startTransaction();

            try {

                //Cambiar de Estado a las tarjetaOficina - pedidoAutomaticoEnviado
                let detalleDto=new DetalleDto();
                detalleDto.solicitud=solicitud._id
                this.logService.log(`detalleDto: ${JSON.stringify(detalleDto)}`);
                let listDetalle= await this.detalleModel.find(detalleDto);
                this.logService.log('Listado de Detalle: '+listDetalle.length);
                if(listDetalle!=null && listDetalle.length>0){
                    this.logService.log('Listado de Detalle: '+listDetalle.length);
                    this.logService.log('Listado de Detalle: '+listDetalle);
                    for (let index = 0; index < listDetalle.length; index++) {
                        const element = listDetalle[index];
                         await this.tarjetaService.modificarPeticionAutoTarjetaOficina(element.tarjeta,solicitud.oficina, false);
                    }
                }
                
                
                solicitud.motivo = solicitudDto.motivo;
                let estadoSolicitado = await this.parametroService.obtenerValorPorCodigo(ctts.keys.estados.ANULADO);
                solicitud.estado = estadoSolicitado;
                solicitud.descripcionMotivo = solicitudDto.descripcionMotivo;
                solicitud.ultimaModificacion = new Date();

                return await solicitud.save();

            } catch (error) {
                await session.abortTransaction();
                this.logService.error('Error al anular la solicitud', error);
            } finally {
                session.endSession();
            }
        } catch (error) {
            this.logService.error('Transaccion no establecida', error);
        }
    }

    async promoverSolicitudesPorEstados(estadoInicial, estadoAPromover) {
        this.logService.log(`ejecuntando actualizacion a estado: ${estadoAPromover}`);
        let solicitudTDO = new SolicitudDto();
        let estadoSolicitudInicial = await this.parametroService.obtenerValorPorCodigo(estadoInicial);

        solicitudTDO.estado = estadoSolicitudInicial._id;
        this.logService.log(`solicitudTDO: ${JSON.stringify(solicitudTDO)}`);
        let solicitudes = await this.solicitudModel.find(solicitudTDO).exec();
        this.logService.log(`Cantidad de solicitudes encontradas en estado[${estadoInicial}]`);
        if (solicitudes != null && solicitudes.length > 0) {
            let estadoSolicitudPromover = await this.parametroService.obtenerValorPorCodigo(estadoAPromover);
            let updated = null;
            for (let index = 0; index < solicitudes.length; index++) {
                updated = new SolicitudDto();
                updated.estado = estadoSolicitudPromover._id;
                updated.ultimaModificacion = new Date();
                await this.solicitudModel.updateOne({ _id: solicitudes[index]._id }, updated);
            }
        }
    }

    async promoverSolicitudesAGestion() {
        this.logService.log('Inicio ejecucion promoverSolicitudesAGestion()');
        await this.promoverSolicitudesPorEstados(ctts.keys.estados.SOLICITADO, ctts.keys.estados.GESTIONADO);
        this.logService.log('Fin ejecucion promoverSolicitudesAGestion()');
    }


    async promoverSolicitudesAProduccion() {
        this.logService.log('Inicio ejecucion promoverSolicitudesAProduccion()');
        await this.promoverSolicitudesPorEstados(ctts.keys.estados.GESTIONADO, ctts.keys.estados.PRODUCCION);
        this.logService.log('Fin ejecucion promoverSolicitudesAProduccion()');
    }

    async generacionSolicitudJob() {
        this.logService.log('Inicio ejecucion generacionSolicitudJob()');
        let listaTarjetaOficina= await this.tarjetaService.listarTarjetasOficinasTotal();
        let oficina ='';
        let solicitudDto= new SolicitudDto();
        let  details = new Array<IDetalle>();
        let detail= new DetalleDto();
        let stockActual;

        if(listaTarjetaOficina!=null && listaTarjetaOficina.length>0){
        

            for (let i = 0; i < listaTarjetaOficina.length; i++) {
                const element = listaTarjetaOficina[i];
                stockActual=await this.tarjetaService.obtenerStockActual(element._id);
                this.logService.log(`stockActual= ${JSON.stringify(stockActual)}`);


                //CONSULTAR STOCK REAL Y COMPARAR
                 if(i==0 && !element.pedidoAutomaticoEnviado && element.automatico==true && Number(stockActual.stock)<Number(element.stockMinimo)){
                    
                    solicitudDto= new SolicitudDto(); 
                    oficina=element.oficina;
                    detail= new DetalleDto();
                    detail.tarjeta=element.tarjeta;
                    detail.cantidad=element.valorAutomatico;
                    details.push(detail);
    
                    if(listaTarjetaOficina.length==1){
                        solicitudDto.oficina=oficina;
                        solicitudDto.details=details;
                        this.logService.log(`solicitudCreate1=${JSON.stringify(solicitudDto)}`);

                        if(details.length>0){
                        //INSERTAR SOLICITUD Y ACTUALIZAR CAMPO DE PETICIONAUTOMATICO
                        let solicitudCreada=await this.crearSolicitudPedido(solicitudDto);
                        //element.pedidoAutomaticoEnviado=true;
                        for (let index = 0; index < details.length; index++) {
                            const obj = details[index];
                            await this.tarjetaService.modificarPeticionAutoTarjetaOficina( obj.tarjeta, oficina, true);
                        }

                        let notificacion = new NotificacionDto();
                        notificacion.mensaje = `Se creo la solicitud ${solicitudCreada.codigo} de forma Automatica.`
                        notificacion.oficina = oficina;
                        await this.notificacionService.crearNotificacion(notificacion);
                        detail= new DetalleDto();
                        details = new Array<IDetalle>();
                        solicitudDto= new SolicitudDto();
                        oficina='';
                        }
                        
                    } 
          
                 }else{
    
                    if(i==listaTarjetaOficina.length-1 ){
                      
                        if(oficina.trim!=element.oficina.trim){
                            solicitudDto= new SolicitudDto();
                            oficina=element.oficina;
                        }
                        if( !element.pedidoAutomaticoEnviado && element.automatico==true && Number(stockActual.stock)<Number(element.stockMinimo)){

                        detail= new DetalleDto();
                        detail.tarjeta=element.tarjeta;
                        detail.cantidad=element.valorAutomatico;
                        details.push(detail);
                        }
                        solicitudDto.oficina=oficina;
                        solicitudDto.details=details;
                        if(details.length>0){
                        //solicitudDto.usuarioAsignado=ctts.usuarioAutomatico; 
                        this.logService.log(`solicitudCreate2=${JSON.stringify(solicitudDto)}`);
                        //INSERTAR SOLICITUD Y ACTUALIZAR CAMPO DE PETICIONAUTOMATICO
                        let solicitudCreada=await this.crearSolicitudPedido(solicitudDto);
                        for (let index = 0; index < details.length; index++) {
                            const obj = details[index];
                             await this.tarjetaService.modificarPeticionAutoTarjetaOficina( obj.tarjeta, oficina, true);
                        }
                        let notificacion = new NotificacionDto();
                        notificacion.mensaje = `Se creo la solicitud ${solicitudCreada.codigo} de forma Automatica.`
                        notificacion.oficina = oficina;
                        await this.notificacionService.crearNotificacion(notificacion);
                        detail= new DetalleDto();
                        details = new Array<IDetalle>();
                        solicitudDto= new SolicitudDto();
                        oficina='';
                        }
                        
                    }else{
                       
                        if(oficina.trim==element.oficina.trim ){
                            if( !element.pedidoAutomaticoEnviado && element.automatico==true && Number(stockActual.stock)<Number(element.stockMinimo)){
                            
                                detail= new DetalleDto();
                                detail.tarjeta=element.tarjeta;
                                detail.cantidad=element.valorAutomatico;
                                details.push(detail);
                            }
                           
                            
                        }else{

                            oficina=element.oficina;
                            solicitudDto.oficina=oficina;
                            solicitudDto.details=details;
                            this.logService.log(`solicitudCreate3=${JSON.stringify(solicitudDto)}`);
                            if(details.length>0){
                            //INSERTAR SOLICITUD Y ACTUALIZAR CAMPO DE PETICIONAUTOMATICO
                            let solicitudCreada=await this.crearSolicitudPedido(solicitudDto);
                            for (let index = 0; index < details.length; index++) {
                                const obj = details[index];
                                await this.tarjetaService.modificarPeticionAutoTarjetaOficina( obj.tarjeta, oficina, true);
                                }
                            let notificacion = new NotificacionDto();
                            notificacion.mensaje = `Se creo la solicitud ${solicitudCreada.codigo} de forma Automatica.`
                            notificacion.oficina = oficina;
                            await this.notificacionService.crearNotificacion(notificacion);
                        
                            detail= new DetalleDto();
                            details = new Array<IDetalle>();
                            solicitudDto= new SolicitudDto();
                            oficina='';
                            }
                            
                        }
    
                    
                    }
                    
                 }
                
            }
        }


        this.logService.log('Fin ejecucion generacionSolicitudJob()');
    } 

}
