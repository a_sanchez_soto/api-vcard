import { Injectable, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { ctts } from '../../util/constants';
import { ITarjeta } from '../../models/interfaces/tarjeta.interface';
import { TarjetaDto } from '../../models/dtos/tarjeta.dto';
import { IMovimiento } from '../../models/interfaces/movimiento.interfcae';
import { ITarjetaOfciina } from '../../models/interfaces/tarjeta-oficina.interface';
import { TarjetaOficinaDto } from '../../models/dtos/tarjeta-oficina.dto';
import { ParametroService } from '../parametro/parametro.service';
import { LogService } from '../../log/log.service';
import { MovimientoDto } from '../../models/dtos/movimiento.dto';
import { NotificacionService } from '../notificacion/notificacion.service';
import { NotificacionDto } from 'src/models/dtos/noticication.dto';

@Injectable()
export class TarjetaService {

    constructor(
        @Inject(ctts.db.models.tarjeta.provide)
        private readonly tarjetaModel: Model<ITarjeta>,
        @Inject(ctts.db.models.movimiento.provide)
        private readonly movimientoModel: Model<IMovimiento>,
        @Inject(ctts.db.models.tarjetaOficina.provide)
        private readonly tarjetaOficinaModel: Model<ITarjetaOfciina>,
        private readonly logService: LogService,
        private readonly parametroService: ParametroService,
        private readonly notificacionService: NotificacionService
    ) { }

    async listarTarjetas() {
        return await this.tarjetaModel.find().exec();
    }

    async obtenerStockActual(idTarjetaOficina) {
        let movimientos = await this.movimientoModel.find({tarjetaOficina: idTarjetaOficina})
                                            .populate(ctts.populates.tipoMovimiento).exec();
        if(movimientos){
            let sum = 0;
            for (let i = 0; i < movimientos.length; i++) {
                const element = movimientos[i];
               
                if(element.tipoMovimiento.valor==ctts.keys.tipoMovimiento.in){
                    sum = sum+element.cantidad;
                }else{
                    sum = sum-element.cantidad;
                }
                 
            }
            return {
                stock: sum
            };
        }
        return {
            stock: 0
        };
    }


    async listarTarjetasOficinasTotal() {
        return await this.tarjetaOficinaModel.find().sort( { oficina: 1 } );

    }

    async listarTarjetasOficinas(tarjetaOficnaDto) {
        return await this.tarjetaOficinaModel.find(tarjetaOficnaDto)
                                               .populate(ctts.populates.tarjeta)
                                               .populate(ctts.populates.oficina)
                                               .populate({
                                                    path: ctts.populates.movimientos,
                                                    populate: {
                                                        path: ctts.populates.tipoMovimiento
                                                    }
                                                })
                                                .sort([['_id', 'descending']])
                                                .exec();
    }

    async crearTarjeta(tarjetaDto: TarjetaDto) {
        let tarjeta = new this.tarjetaModel(tarjetaDto);
        return await tarjeta.save();
    }

    async obtenerTarjetaPorCodigo(codigo) {
        return await this.tarjetaModel.findOne({ codigo: codigo });
    }

    async obtenerTarjetaOficina(idTarjeta, idOficina) {
        return await this.tarjetaOficinaModel.findOne({ 
            tarjeta: idTarjeta,
            oficina: idOficina 
        });
    }

    async crearTarjetaOficina(tarjetaOficinaDto) {
        let tarjetaOficina = new this.tarjetaOficinaModel(tarjetaOficinaDto);
        await tarjetaOficina.save();
        return tarjetaOficina;
    }

    async agregarMovimientoTarjetaOficina(tarjetaOficina, movimiento) {
        this.logService.log(`Movminientos encontrados tarjetaOficina[${tarjetaOficina._id}]: ${JSON.stringify(tarjetaOficina.movimientos)}`);
        let listMovimientos = [];
        for (let i = 0; i < tarjetaOficina.movimientos.length; i++) {
            const element = tarjetaOficina.movimientos[i];
            this.logService.log(`tarjetaOficina.movimientos[${i}]: ${JSON.stringify(element)}`);
            listMovimientos.push(element._id);   
        }
        listMovimientos.push(movimiento._id);
        let tarjetaOficinaDto = new TarjetaOficinaDto();
        tarjetaOficinaDto.movimientos = listMovimientos;
        this.logService.log(`Actualizando movminientos tarjetaOficina[${tarjetaOficina._id}]: ${JSON.stringify(tarjetaOficinaDto)}`);
        return await this.tarjetaOficinaModel.findOneAndUpdate( { _id: tarjetaOficina._id } , tarjetaOficinaDto);
    }

    async crearMovimiento(movimientoDto) {
        movimientoDto.creacion = new Date();
        let  movimiento = new this.movimientoModel(movimientoDto);
        await movimiento.save();
        return movimiento;
    }

    async listarMovimientos(idTarjetaOficina) {
        return await this.movimientoModel.find({tarjetaOficina: idTarjetaOficina})
                    .populate(ctts.populates.tipoMovimiento)
                    .populate({
                        path: ctts.populates.tarjetaOficina,
                        populate: {
                            path: ctts.populates.tarjeta
                        }
                    })
                    .exec();
    }

    async modificarPeticionAutoTarjetaOficina( tarjeta,  oficina ,estadoPeticion) {
        let updated = new TarjetaOficinaDto();
        updated.pedidoAutomaticoEnviado = estadoPeticion;
        await this.tarjetaOficinaModel.findOneAndUpdate({oficina: oficina, tarjeta:tarjeta}, updated).populate(ctts.populates.tarjeta);
    }
    async modificarTarjetaOficina(tarjetaOficinaDto) {
        let updated = new TarjetaOficinaDto();
        if(typeof tarjetaOficinaDto.stockMinimo !== 'undefined'){
            updated.stockMinimo = tarjetaOficinaDto.stockMinimo;
        }
        if(typeof tarjetaOficinaDto.automatico !== 'undefined'){
            updated.automatico = tarjetaOficinaDto.automatico;
        }
        if(typeof tarjetaOficinaDto.valorAutomatico !== 'undefined'){
            updated.valorAutomatico = tarjetaOficinaDto.valorAutomatico;
        }
        if(typeof tarjetaOficinaDto.pedidoAutomaticoEnviado !== 'undefined'){
            updated.pedidoAutomaticoEnviado = tarjetaOficinaDto.pedidoAutomaticoEnviado;
        }
        
        let tarjetaOficina = await this.tarjetaOficinaModel.findOneAndUpdate({_id: tarjetaOficinaDto._id}, updated)
                                    .populate(ctts.populates.tarjeta)
                                    .populate(ctts.populates.oficina);
        
                                                            
        if(typeof tarjetaOficinaDto.stockMinimo !== 'undefined'){
            this.logService.log('trae stock minimo');    
            let stockActual = await this.obtenerStockActual(tarjetaOficinaDto._id);
            this.logService.log(`STock Actual= ${stockActual.stock}`);  
            this.logService.log(`tarjetaOficinaDto.stockMinimo= ${tarjetaOficinaDto.stockMinimo}`);   
            if(stockActual.stock <= tarjetaOficinaDto.stockMinimo) {
                let notificacion = new NotificacionDto();
                notificacion.mensaje = `El stock de la tarjeta ${tarjetaOficina.tarjeta.value} esta por debajo del mínimo.`
                notificacion.oficina = tarjetaOficina.oficina._id;
                await this.notificacionService.crearNotificacion(notificacion);
            }
        }

        // 

        if(typeof tarjetaOficinaDto.rango !== 'undefined') {
             let tarjetaDto = new TarjetaDto();
             tarjetaDto.rango = tarjetaOficinaDto.rango;
             await this.tarjetaModel.updateOne({_id : tarjetaOficina.tarjeta._id}, tarjetaDto); 
        }

        return tarjetaOficina;
    }

    async adquirirTarjeta(tarjetaOficinaDto) {
        this.logService.log(`Movminientos encontrados tarjetaOficina[:`);
        //Se registra movimiento de ingreso(+)
        let movimientoDto = null;
        let movimiento=null;
        let tipoMovimientoIngreso = await this.parametroService.obtenerValorPorCodigo(ctts.keys.tipoMovimiento.out);
        let tarjetaOficina= await this.obtenerTarjetaOficina(tarjetaOficinaDto.tarjeta,tarjetaOficinaDto.oficina);
        movimientoDto = new MovimientoDto();
        movimientoDto.tipoMovimiento = tipoMovimientoIngreso._id;
        movimientoDto.cantidad = tarjetaOficinaDto.valorAutomatico;
        movimientoDto.tarjetaOficina = tarjetaOficina._id;
        movimiento = await this.crearMovimiento(movimientoDto);
         
        this.logService.log(`Movimiento creado: ${JSON.stringify(movimiento)}`);

        //REGISTRAR MOVIMIENTO
        return await this.agregarMovimientoTarjetaOficina(tarjetaOficina, movimiento)
    }

}
