import * as mongoose from 'mongoose';
import { ctts } from '../util/constants';


export const SequenceSchema = new mongoose.Schema({
    _id: {type: String, required: true},
    sequence_value: {type: Number, default: 1}
});

export const UsuarioSchema = new mongoose.Schema({
    userName: String,
    password: String,
    nombreCompleto: String,
    registro: String,
    email: String,
    estado: { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.valor.entityName },
    perfil: { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.valor.entityName },
    oficina: { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.valor.entityName },
    territorio: { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.valor.entityName },
    opciones: [ { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.valor.entityName } ]
});

export const ListaSchema = new mongoose.Schema({
    codigo: String,
    descripcion: String,
    estado: String,
    creacion: Date,
    ultimaModificacion: Date
});

export const ValorSchema = new mongoose.Schema({
    codigo: String,
    nombre: String,
    contenido: String,
    valor: String,
    valorNumerico: Number,
    lista: { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.lista.entityName }
});

export const TarjetaSchema = new mongoose.Schema({
    bin: String,
    codigo: String,
    descripcion: String,
    value: String,
    formato: String,
    imagen: String,
    rango: String
});

export const TarjetaOficinaSchema = new mongoose.Schema({
    tarjeta: { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.tarjeta.entityName },
    oficina: { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.valor.entityName },
    stock: Number,
    stockMinimo: Number,
    automatico: Boolean,
    valorAutomatico:Number,
    pedidoAutomaticoEnviado:Boolean,
    movimientos: [ { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.movimiento.entityName } ]
});

export const SolicitudSchema = new mongoose.Schema({
    codigo: String,
    estado: { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.valor.entityName },
    oficina: { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.valor.entityName },
    territorio: { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.valor.entityName },
    usuarioCreador: { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.usuario.entityName },
    usuarioAsignado: { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.usuario.entityName },
    creacion: Date,
    ultimaModificacion: Date,
    details: [ { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.detalle.entityName } ],
    motivo: { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.valor.entityName },
    descripcionMotivo: String,
});

export const DetalleSchema = new mongoose.Schema({
    tarjeta: { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.tarjeta.entityName },
    cantidad: Number,
    solicitud: { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.solicitud.entityName },
});

export const MovimientoSchema = new mongoose.Schema({
    tarjetaOficina: { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.tarjetaOficina.entityName },
    tipoMovimiento: { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.valor.entityName },
    cantidad: Number,
    creacion: Date
});

export const NotificacionSchema = new mongoose.Schema({
    mensaje: String,
    visto: String,
    oficina: { type: mongoose.Schema.Types.ObjectId, ref: ctts.db.models.valor.entityName },
    creacion: Date
});