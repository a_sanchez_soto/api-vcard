import { Connection } from 'mongoose';
import { ctts } from '../../util/constants';
import { TarjetaSchema } from '../models.schemas';

export const TarjetaProvider = [
    {
        provide: ctts.db.models.tarjeta.provide,
        useFactory: (connection: Connection) => connection.model(ctts.db.models.tarjeta.entityName, TarjetaSchema),
        inject: [ctts.db.cnxName]
    }
];