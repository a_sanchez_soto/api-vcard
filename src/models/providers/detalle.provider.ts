import { Connection } from 'mongoose';
import { ctts } from '../../util/constants';
import { DetalleSchema } from '../models.schemas';

export const DetalleProvider = [
    {
        provide: ctts.db.models.detalle.provide,
        useFactory: (connection: Connection) => connection.model(ctts.db.models.detalle.entityName, DetalleSchema),
        inject: [ctts.db.cnxName]
    }
];