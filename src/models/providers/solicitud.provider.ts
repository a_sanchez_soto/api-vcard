import { Connection } from 'mongoose';
import { ctts } from '../../util/constants';
import { SolicitudSchema } from '../models.schemas';

export const SolicitudProvider = [
    {
        provide: ctts.db.models.solicitud.provide,
        useFactory: (connection: Connection) => connection.model(ctts.db.models.solicitud.entityName, SolicitudSchema),
        inject: [ctts.db.cnxName]
    }
];