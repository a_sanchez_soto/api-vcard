import { Connection } from 'mongoose';
import { ctts } from '../../util/constants';
import { TarjetaOficinaSchema } from '../models.schemas';

export const TarjetaOficinaProvider = [
    {
        provide: ctts.db.models.tarjetaOficina.provide,
        useFactory: (connection: Connection) => connection.model(ctts.db.models.tarjetaOficina.entityName, TarjetaOficinaSchema),
        inject: [ctts.db.cnxName]
    }
];