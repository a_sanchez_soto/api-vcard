import { Connection } from 'mongoose';
import { ctts } from '../../util/constants';
import { ListaSchema } from '../models.schemas';

export const ListaProvider = [
    {
        provide: ctts.db.models.lista.provide,
        useFactory: (connection: Connection) => connection.model(ctts.db.models.lista.entityName, ListaSchema),
        inject: [ctts.db.cnxName]
    }
];