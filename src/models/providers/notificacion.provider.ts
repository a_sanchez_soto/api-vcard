import { Connection } from 'mongoose';
import { ctts } from '../../util/constants';
import { NotificacionSchema } from '../models.schemas';

export const NotificacionProvider = [
    {
        provide: ctts.db.models.notificacion.provide,
        useFactory: (connection: Connection) => connection.model(ctts.db.models.notificacion.entityName, NotificacionSchema),
        inject: [ctts.db.cnxName]
    }
];