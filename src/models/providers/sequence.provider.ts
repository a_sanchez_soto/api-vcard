import { Connection } from 'mongoose';
import { ctts } from '../../util/constants';
import { SequenceSchema } from '../models.schemas';

export const SequenceProvider = [
    {
        provide: ctts.db.models.sequence.provide,
        useFactory: (connection: Connection) => connection.model(ctts.db.models.sequence.entityName, SequenceSchema),
        inject: [ctts.db.cnxName]
    }
];