import { Connection } from 'mongoose';
import { ctts } from '../../util/constants';
import { MovimientoSchema } from '../models.schemas';

export const MovimientoProvider = [
    {
        provide: ctts.db.models.movimiento.provide,
        useFactory: (connection: Connection) => connection.model(ctts.db.models.movimiento.entityName, MovimientoSchema),
        inject: [ctts.db.cnxName]
    }
];