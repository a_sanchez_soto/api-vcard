import { Connection } from 'mongoose';
import { ctts } from '../../util/constants';
import { ValorSchema } from '../models.schemas';

export const ValorProvider = [
    {
        provide: ctts.db.models.valor.provide,
        useFactory: (connection: Connection) => connection.model(ctts.db.models.valor.entityName, ValorSchema),
        inject: [ctts.db.cnxName]
    }
];