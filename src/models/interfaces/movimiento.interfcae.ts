import { Document } from 'mongoose';

export interface IMovimiento extends Document {
    readonly tarjetaOficina: string;
    readonly tipoMovimiento: string;
    readonly cantidad: number;
    readonly creacion: Date;
}