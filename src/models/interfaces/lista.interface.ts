import { Document } from 'mongoose';

export interface ILista extends Document {
    readonly codigo: string;
    readonly descripcion: string;
    readonly estado: string
    readonly creacion: Date;
    readonly ultimaModificacion: Date;
}