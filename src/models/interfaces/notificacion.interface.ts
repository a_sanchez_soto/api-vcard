import { Document } from 'mongoose';

export interface INotificacion extends Document {
    readonly oficina: string;
    readonly mensaje: string;
    readonly visto: string;
    readonly creacion: Date;
}