import { Document } from 'mongoose';
import { IMovimiento } from './movimiento.interfcae';

export interface ITarjetaOfciina extends Document {
    readonly tarjeta: string;
    readonly oficina: string;
    readonly stock: number;
    readonly stockMinimo: number;
    readonly automatico: boolean;
    readonly valorAutomatico: number;
    readonly pedidoAutomaticoEnviado: boolean;
    readonly movimientos: Array<IMovimiento>;
}