import { Document } from 'mongoose';

export interface ITarjeta extends Document {
    readonly bin: string;
    readonly codigo: string;
    readonly descripcion: string;
    readonly value: string;
    readonly formato: string;
    readonly imagen: string;
    readonly rango: string;
    readonly stock: number;
    readonly stockMinimo: number;
    readonly automatico: boolean;
    readonly valorAutomatico: number;
}