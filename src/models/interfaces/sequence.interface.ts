import { Document } from 'mongoose';

export interface ISequence extends Document {
    readonly _id: string;
    readonly sequence_value: number;
}