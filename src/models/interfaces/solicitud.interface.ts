import { Document } from 'mongoose';
import { IDetalle } from './detalle.interface';

export interface ISolicitud extends Document {
    readonly codigo: number;
    readonly estado: string;
    readonly oficina: string;
    readonly territorio: string;
    readonly usuarioCreador: string;
    readonly usuarioAsignado: string;
    readonly creacion: Date;
    readonly ultimaModificacion: Date;
    readonly details: Array<IDetalle>;
    readonly motivo:String,
    readonly descripcionMotivo: String
}