import { Document } from 'mongoose';

export interface IDetalle extends Document {
    readonly _id: string;
    readonly cantidad: number;
    readonly tarjeta: string;
    solicitud: string;
}