import { Document } from 'mongoose';
import { IValor } from './valor.interface';

export interface IUsuario extends Document {
    readonly userName: string;
    readonly password: string;
    readonly nombreCompleto: string;
    readonly registro: string;
    readonly perfil: string;
    readonly oficina: string;
    readonly territorio: string;
    readonly email: string;
    readonly estado: string;
    readonly opciones: Array<IValor>;
}
 