import { Document } from 'mongoose';

export interface IValor extends Document {
    readonly codigo: string;
    readonly nombre: string;
    readonly contenido: string;
    readonly valor: string;
    readonly valorNumerico: number;
    readonly lista: string;
}