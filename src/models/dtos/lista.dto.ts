import { ApiModelProperty } from "@nestjs/swagger";

export class ListaDto {
    
    @ApiModelProperty({ description: 'id de la lista' })
    _id: string;

    @ApiModelProperty({ description: 'codigo de la lista' })
    codigo: string;

    @ApiModelProperty({ description: 'descripcion de la lista' })
    descripcion: string;
    
    @ApiModelProperty({ description: 'estado de la lista' })
    estado: string;
    
    @ApiModelProperty({ description: 'fecha de creacion' })
    creacion: Date;
    
    @ApiModelProperty({ description: 'fecha de la ultima modificacion' })
	ultimaModificacion: Date;

}