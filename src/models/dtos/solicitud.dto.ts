import { ApiModelProperty } from "@nestjs/swagger";
import { IDetalle } from "../interfaces/detalle.interface";

export class SolicitudDto {
    
    @ApiModelProperty({ description: 'id de la solicitud' })
    _id: string;

    @ApiModelProperty({ description: 'codigo de la solicitud' })
    codigo: number;

    @ApiModelProperty({ description: 'estado de la solicitud' })
    estado: string;
    
    @ApiModelProperty({ description: 'oficina a la que pertenece la solicitud' })
    oficina: string;

    @ApiModelProperty({ description: 'territorio al que pertenece la solicitud' })
    territorio: string;

    @ApiModelProperty({ description: 'usuario que crea la solicitud' })
    usuarioCreador: string;

    @ApiModelProperty({ description: 'usuario responsable actual de la solicitud' })
    usuarioAsignado: string;
    
    @ApiModelProperty({ description: 'fecha de creacion' })
    creacion: Date;
    
    @ApiModelProperty({ description: 'fecha de la ultima modificacion' })
    ultimaModificacion: Date;
    
    @ApiModelProperty({ description: 'detalle de la solicitud' })
    detalle: Array<IDetalle>;
    
    @ApiModelProperty({ description: 'Perfil de usuario' })
    perfil: string;

    @ApiModelProperty({ description: 'Codigo de Motivo' })
    motivo: string;

    @ApiModelProperty({ description: 'Descripcion de Motivo' })
    descripcionMotivo: string;

    @ApiModelProperty({ description: 'Detalle de la solicitud' })
	details: Array<IDetalle>;

    @ApiModelProperty({ description: 'idUsuario para filtros' })
    idUsuario: string;

}