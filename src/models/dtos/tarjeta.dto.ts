import { ApiModelProperty } from "@nestjs/swagger";

export class TarjetaDto {
    
    @ApiModelProperty({ description: 'id de la tarjeta' })
    _id: string;

    @ApiModelProperty({ description: 'bin de la tarjeta' })
    bin: string;

    @ApiModelProperty({ description: 'codigo de la tarjeta' })
    codigo: string;

    @ApiModelProperty({ description: 'descripcion de la tarjeta' })
    descripcion: string;

    @ApiModelProperty({ description: 'valor de la tarjeta' })
    value: string;

    @ApiModelProperty({ description: 'tipo de formato' })
    formato: string;

    @ApiModelProperty({ description: 'imagen de la tarjeta' })
    imagen: string;

    @ApiModelProperty({ description: 'rango de la tarjeta' })
    rango: string;

}