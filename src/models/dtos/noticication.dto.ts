import { ApiModelProperty } from "@nestjs/swagger";

export class NotificacionDto {
   
    @ApiModelProperty({ description: 'id de la notificacion' })
    _id: string;

    @ApiModelProperty({ description: 'Oficina a la que pertenece la notificacion' })
    oficina: string;

    @ApiModelProperty({ description: 'Mensaje de la notificacion' })
    mensaje: string;

    @ApiModelProperty({ description: 'indicador de visualizacion de la notificacion' })
    visto: string;

    @ApiModelProperty({ description: 'Fecha de creacion' })
    creacion: Date;

}