import { ApiModelProperty } from "@nestjs/swagger";

export class DetalleDto {
   
    @ApiModelProperty({ description: 'id del detalle' })
    _id: string;

    @ApiModelProperty({ description: 'tarjeta asociada a la solicitud de detalle' })
    tarjeta: string;

    @ApiModelProperty({ description: 'Cantidad de tarjetas solicitadas' })
    cantidad: number;

    @ApiModelProperty({ description: 'Solicitud de detalle' })
    solicitud: string;

}