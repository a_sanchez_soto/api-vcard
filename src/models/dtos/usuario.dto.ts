import { ApiModelProperty } from "@nestjs/swagger";
import { IValor } from "../interfaces/valor.interface";

export class UsuarioDto {
    @ApiModelProperty({ description: 'id del usuario' })
    _id: string;

    @ApiModelProperty({ description: 'nombre de usuario' })
    userName: string;

    @ApiModelProperty({ description: 'contraseña de acceso del usuario' })
    password: string;

    @ApiModelProperty({ description: 'Nombre Comppleto de la persona' })
    nombreCompleto: string;

    @ApiModelProperty({ description: 'registro del usuario' })
    registro: string;

    @ApiModelProperty({ description: 'perfil del usuario' })
    perfil: string;

    @ApiModelProperty({ description: 'oficina del usuario' })
    oficina: string;

    @ApiModelProperty({ description: 'territorio del usuario' })
    territorio: string;

    @ApiModelProperty({ description: 'Correo electronico del usuario' })
    email: string;

    @ApiModelProperty({ description: 'Estado del usuario' })
    estado: string;

    @ApiModelProperty({ description: 'Opciones del sistema' })
    opciones: Array<IValor>;

}