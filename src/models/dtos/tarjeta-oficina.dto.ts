import { ApiModelProperty } from "@nestjs/swagger";
import { IMovimiento } from "../interfaces/movimiento.interfcae";

export class TarjetaOficinaDto {
    
    @ApiModelProperty({ description: 'id tarjeta oficina' })
    _id: string;

    @ApiModelProperty({ description: 'tarjeta' })
    tarjeta: string;

    @ApiModelProperty({ description: 'oficina' })
    oficina: string;

    @ApiModelProperty({ description: 'stock disponible de la tarjeta' })
    stock: number;

    @ApiModelProperty({ description: 'stock minimo aceptado' })
    stockMinimo: number;
    
    @ApiModelProperty({ description: 'pedido automatico cuando stock menor al minimo' })
    automatico: boolean;

    @ApiModelProperty({ description: 'cantidad del pedido automatico' })
    valorAutomatico: number;

    @ApiModelProperty({ description: 'envio de pedido automatico activado' })
    pedidoAutomaticoEnviado: boolean;

    @ApiModelProperty({ description: 'Movimientos de la tarjeta en la oficina' })
	movimientos: Array<IMovimiento>;

    @ApiModelProperty({ description: 'Rango de la tarjeta' })
    rango: number;

}