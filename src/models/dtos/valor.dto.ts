import { ApiModelProperty } from '@nestjs/swagger';
import { ListaDto } from './lista.dto';

export class ValorDto {
    @ApiModelProperty({ description: 'id de valor' })
    _id: string;

    @ApiModelProperty({ description: 'código del valor' })
    codigo: string;

    @ApiModelProperty({ description: 'nombre del valor' })
    nombre: string;

    @ApiModelProperty({ description: 'contenido del valor' })
    contenido: string;

    @ApiModelProperty({ description: 'valor general' })
    valor: string;

    @ApiModelProperty({ description: 'valor general' })
    valorNumerico: number;

    @ApiModelProperty({ description: 'id de la lista' })
    idLista: number;

    @ApiModelProperty({ description: 'Objeto lista' })
    lista: ListaDto;

}