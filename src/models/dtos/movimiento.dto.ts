import { ApiModelProperty } from "@nestjs/swagger";

export class MovimientoDto {
    @ApiModelProperty({ description: 'id del movimiento' })
    _id: string;

    @ApiModelProperty({ description: 'tarjeta asociada al movimiento' })
    tarjetaOficina: string;

    @ApiModelProperty({ description: 'codigo de la tarjeta asociada al movimiento' })
    tipoMovimiento: string;

    @ApiModelProperty({ description: 'Cantidad del movimiento' })
    cantidad: number;

    @ApiModelProperty({ description: 'fecha de creacion' })
    creacion: Date;
    
}