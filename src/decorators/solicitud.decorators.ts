import { createParamDecorator } from "@nestjs/common";

export const Solicitud = createParamDecorator((data: string, req) => {
    return data ? req.query[data] : req.query;
  });