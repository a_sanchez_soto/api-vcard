import { createParamDecorator } from "@nestjs/common";

export const TarjetaOficina = createParamDecorator((data: string, req) => {
    return data ? req.query[data] : req.query;
  });