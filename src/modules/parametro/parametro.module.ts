import { Module, Global } from '@nestjs/common';
import { DatabaseModule } from '../../config/database/database.module';
import { ParametroController } from '../../controllers/parametro/parametro.controller';
import { ParametroService } from '../../services/parametro/parametro.service';
import { ListaProvider } from '../../models/providers/lista.provider';
import { ValorProvider } from '../../models/providers/valor.provider';

@Global()
@Module({
    imports: [DatabaseModule],
    controllers: [ParametroController],
    providers: [ParametroService, ...ListaProvider, ...ValorProvider],
    exports: [ParametroService]
})
export class ParametroModule {}
