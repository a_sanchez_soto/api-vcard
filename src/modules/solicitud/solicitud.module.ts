import { Module, Global } from '@nestjs/common';
import { DatabaseModule } from '../../config/database/database.module';
import { SolicitudController } from '../../controllers/solicitud/solicitud.controller';
import { SolicitudService } from '../../services/solicitud/solicitud.service';
import { SolicitudProvider } from '../../models/providers/solicitud.provider';
import { MovimientoProvider } from '../../models/providers/movimiento.provider';
import { DetalleProvider } from '../../models/providers/detalle.provider';
import { AppGateway } from '../../gateways/app.gateway';

@Global()
@Module({
    imports: [DatabaseModule],
    controllers: [SolicitudController],
    providers: [SolicitudService, ...SolicitudProvider, ...MovimientoProvider, ...DetalleProvider, AppGateway],
    exports: [SolicitudService]
})
export class SolicitudModule {}
