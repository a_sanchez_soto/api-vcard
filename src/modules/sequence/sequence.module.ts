import { Module, Global } from '@nestjs/common';
import { SequenceService } from '../../services/sequence/sequence.service';
import { SequenceProvider } from '../../models/providers/sequence.provider';
import { DatabaseModule } from '../../config/database/database.module';

@Global()
@Module({
  imports:[DatabaseModule],  
  providers: [SequenceService, ...SequenceProvider],
  exports: [SequenceService],
})
export class SequenceModule {}
