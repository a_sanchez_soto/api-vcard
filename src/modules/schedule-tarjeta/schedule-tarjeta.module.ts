import { Module } from '@nestjs/common';
import { TarjetaScheduleService } from '../../jobs/tarjeta-schedule/tarjeta-schedule.service';
@Module({
    providers: [TarjetaScheduleService],
    exports:[TarjetaScheduleService]
})

export class ScheduleTarjetaModule { }
