import { Module, Global } from '@nestjs/common';
import { DatabaseModule } from '../../config/database/database.module';
import { TarjetaController } from '../../controllers/tarjeta/tarjeta.controller';
import { TarjetaService } from '../../services/tarjeta/tarjeta.service';
import { TarjetaProvider } from '../../models/providers/tarjeta.provider';
import { MovimientoProvider } from '../../models/providers/movimiento.provider';
import { TarjetaOficinaProvider } from '../../models/providers/tarjeta-oficina.provider';
@Global()
@Module({
    imports: [DatabaseModule],
    controllers: [TarjetaController],
    providers: [TarjetaService, ...TarjetaProvider, ...MovimientoProvider, ...TarjetaOficinaProvider],
    exports: [TarjetaService]
})
export class TarjetaModule {}
