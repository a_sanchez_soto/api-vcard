import { Module, Global } from '@nestjs/common';
import { DatabaseModule } from '../../config/database/database.module';
import { NotificacionService } from '../../services/notificacion/notificacion.service';
import { NotificacionProvider } from '../../models/providers/notificacion.provider';
import { NotificacionController } from '../../controllers/notificacion/notificacion.controller';
import { AppGateway } from '../../gateways/app.gateway';

@Global()
@Module({
    imports: [DatabaseModule],
    providers: [NotificacionService, ...NotificacionProvider, AppGateway],
    exports: [NotificacionService],
    controllers: [NotificacionController]
})
export class NotificacionModule {}
