import { SubscribeMessage, WebSocketGateway, OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect, WsResponse, WebSocketServer } from '@nestjs/websockets';
import { LogService } from '../log/log.service';
import { Socket, Server } from 'socket.io';

@WebSocketGateway()
export class AppGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {

  @WebSocketServer()
  wsServer: Server;

  constructor(
    private readonly logService: LogService
  ) { }

  afterInit(server: any) {
    this.logService.log("afterInit.");
  }

  handleConnection(client: Socket, ...args: any[]) {
    this.logService.log(`handleConnection=${client.id}`);
  }

  handleDisconnect(client: Socket) {
    this.logService.log(`handleDisconnect=${client.id}`);
  }

  @SubscribeMessage('messageToServer')
  handleMessage(client: Socket, body: string): void {
    this.logService.log(`body message: ${body}`);
    this.wsServer.emit('messageToClient', body);
  }
}
