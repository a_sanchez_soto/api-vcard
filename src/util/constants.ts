export const ctts = {
    db: {
        url: 'URL_MONGO_DB',
        cnxName:'DATABASE_CONNECTION',
        models: {
            sequence: {
                provide: 'SEQUENCE_MODEL',
                entityName: 'Sequence'
            },
            usuario: {
                provide: 'USUARIO_MODEL',
                entityName: 'Usuario'
            },
            lista: {
                provide: 'LISTA_MODEL',
                entityName: 'Lista'
            },
            valor: {
                provide: 'VALOR_MODEL',
                entityName: 'Valor'
            },
            tarjeta: {
                provide: 'TARJETA_MODEL',
                entityName: 'Tarjeta'
            },
            solicitud: {
                provide: 'SOLICITUD_MODEL',
                entityName: 'Solicitud'
            },
            detalle: {
                provide: 'DETALLE_MODEL',
                entityName: 'Detalle'
            },
            movimiento: {
                provide: 'MOVIMIENTO_MODEL',
                entityName: 'Movimiento'
            },
            tarjetaOficina: {
                provide: 'TARJETA_OFICINA_MODEL',
                entityName: 'TarjetaOficina'
            },
            notificacion: {
                provide: 'NOTIFICACION_MODEL',
                entityName: 'Notificacion'
            }
        }
    },
    jwt: {
        secret: 'coreApiPersona$...'
    },
    keys: {
        expiresIn: '864000s',
        hashLevelBcrypt: 12,
        strategyJwtName: 'jwt',
        strategyLocalName: 'local',
        estados: {
            SOLICITADO: 'SOLICITADO',
            GESTIONADO: 'GESTIONADO',
            PRODUCCION: 'PRODUCCION',
            PORENTREGAR: 'PORENTREGAR',
            ENTREGADO: 'ENTREGADO',
            ANULADO: 'ANULADO'
        },
        oficinas:   "OFICINAS",
        perfiles: {
            ADMIN: "ADMIN_PERFIL",
            OFICINA: "OFICIN_PERFIL",
            PROVEEDOR: "PROVE_PERFIL"
        },
        tipoMovimiento: {
            in:'MOV_IN',
            out: 'MOV_OUT'
        }
    },
    states: {
        on: 'A',
        off: 'I'
    },
    usuarioAutomatico:'automatico',
    populates: {
        estado: 'estado',
        territorio:'territorio',
        oficina: 'oficina',
        usuarioCreador: 'usuarioCreador',
        usuarioAsignado: 'usuarioAsignado',
        details: 'details',
        tarjeta: 'tarjeta',
        perfil:'perfil',
        motivo:'motivo',
        lista: 'lista',
        movimientos:'movimientos',
        tipoMovimiento: 'tipoMovimiento',
        tarjetaOficina: "tarjetaOficina",
        opciones: "opciones"
    }
}; 