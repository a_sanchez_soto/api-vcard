import { Injectable } from '@nestjs/common';
import { Cron, NestSchedule, Interval } from 'nest-schedule';
import { LogService } from '../../log/log.service';
import { SolicitudService } from '../../services/solicitud/solicitud.service';

/* 
    Expresiones para schedulers cron
    posiciones de derecha a izquierda
    ---------------------------------
    SEG | MIN | HOR | DIA | MES | AÑO
     *     *     *     *     *     *
    ---------------------------------
     * = todos (AÑO, MES, DIA ...)
    ---------------------------------
    Ejemplo 01: ejecutar un cron exactamente a las 9 PM
    todos los dias de todos los meses de todos los años
    0 0 9 * * *
    ---------------------------------
    Ejemplo 02: ejecutar un cron cada 10 segundos
    todos los dias de todos los meses de todos los años
    * / 10 * * * * *
    Nota: No tener encuenta los espacions en blanco entre "/"
*/
@Injectable()
export class TarjetaScheduleService extends NestSchedule {
    
    constructor(
        private readonly logService: LogService,
        private readonly solicitudService: SolicitudService
    ) {
        super();
    }

    /**
     * @description se ejecuta a las 5PM
     */
    @Cron('0 0 17 * * *')
    async actualizarEstadoGestionadoJob() {
        this.logService.log(`Se ejecuta el job[actualizarEstadoGestionadoJob]: ${new Date()}`);
        this.solicitudService.promoverSolicitudesAGestion();
    }

    /**
     * @description se ejecuta a la 1AM
     */
    @Cron('0 0 1 * * *')
    async actualizarEstadoProduccionJob() {
        this.logService.log(`Se ejecuta el job[actualizarEstadoProduccionJob]: ${new Date()}`);
        this.solicitudService.promoverSolicitudesAProduccion();
    }

    /**
     * @description se ejecuta generacion Solicitud Automaticamente
     */
    /*
     @Cron('10 * * * * *')
    async generacionSolicitudJob() {
        this.logService.log(`Se ejecuta el job[generacionSolicitudJob]: ${new Date()}`);
        this.solicitudService.generacionSolicitudJob();
    }
    */

}
