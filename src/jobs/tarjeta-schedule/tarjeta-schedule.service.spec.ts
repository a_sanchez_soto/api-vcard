import { Test, TestingModule } from '@nestjs/testing';
import { TarjetaScheduleService } from './tarjeta-schedule.service';

describe('TarjetaScheduleService', () => {
  let service: TarjetaScheduleService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TarjetaScheduleService],
    }).compile();

    service = module.get<TarjetaScheduleService>(TarjetaScheduleService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
